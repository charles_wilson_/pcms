package webdriverbasics.aug12_08_2015;

import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;

public class Profile {

	public Profile(String Company, int a){
		System.out.println("Selenium");
		System.out.println(Company);
	}

	public static void main(String[] args) throws InterruptedException, Exception {
		// TODO Auto-generated method stub

		// WebDriverBasics a=new WebDriverBasics("abcd", 34);
		
		FirefoxDriver fdObj = new FirefoxDriver ();  ///launching browser
		fdObj.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		fdObj.manage().window().maximize();

		fdObj.get("http://qa-pcms.delhivery.com");  ///Opening URL
		
		WebElement uname_we=fdObj.findElementByName("email");
		uname_we.clear();    //// clearing username field
		uname_we.sendKeys("charles.wilson");   /// Enter Username
		Thread.sleep(2000);		
		
		fdObj.findElementByXPath("//input[@name='password']").clear();  // clearing password field
		fdObj.findElementByXPath("//input[@name='password']").sendKeys("pcms123");   /// Enter password
		Thread.sleep(2000);
		
		fdObj.findElementByXPath("//input[@value='login']").click();  // Click Login
		Thread.sleep(2000);

		fdObj.findElementByXPath("//*[@value='1']").click();  // selecting Godam Application Account from drop down
		Thread.sleep(2000);
		fdObj.findElementByXPath("//*[@class='btn btn-lg btn-default btn-block']").click(); // click Submit
	      
		fdObj.findElementByXPath("/html/body/div/nav/div[2]/ul/li[2]/a").click(); //click Profile
        Thread.sleep(2000);

		fdObj.findElementByXPath("/html/body/div/div/div/div/div/div/div/div[1]/h4/a").click(); //click Profile under 'Profile Details'
        Thread.sleep(2000);
		
		fdObj.findElementByXPath("/html/body/div/div/div/div/div/div/div/div[3]/div[1]/h4/a").click(); //click Account Details
		
		Thread.sleep(5000);
		fdObj.close();
		
		}

}

	