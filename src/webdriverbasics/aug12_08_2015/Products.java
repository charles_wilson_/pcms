package webdriverbasics.aug12_08_2015;

import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.lang.*;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;


public class Products {
private String urlInput;
public Products(String Company, int a){
	System.out.println("Selenium");
	System.out.println(Company);
}

public static void main(String[] args) throws InterruptedException, Exception {
	// TODO Auto-generated method stub

	// WebDriverBasics a=new WebDriverBasics("abcd", 34);
	
	FirefoxDriver fdObj = new FirefoxDriver ();  ///launching browser
	fdObj.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
	fdObj.manage().window().maximize();

	fdObj.get("http://qa-pcms.delhivery.com");  ///Opening URL
	
	WebElement uname_we=fdObj.findElementByName("email");
	uname_we.clear();    //// clearing username field
	uname_we.sendKeys("charles.wilson");   /// Enter Username
	Thread.sleep(2000);
	
	fdObj.findElementByXPath("//input[@name='password']").clear();  // clearing password field
	fdObj.findElementByXPath("//input[@name='password']").sendKeys("pcms123");   /// Enter password
	Thread.sleep(2000);
	
	fdObj.findElementByXPath("//input[@value='login']").click();  // Click Login
	Thread.sleep(2000);

	fdObj.findElementByXPath("//*[@value='1']").click();  // selecting Godam Application Account from drop down
	Thread.sleep(2000);
	fdObj.findElementByXPath("//*[@class='btn btn-lg btn-default btn-block']").click(); // click Submit
	Thread.sleep(2000);

	fdObj.findElementByXPath("/html/body/div/nav/div[2]/ul/li[6]/a").click(); //click Products
	Thread.sleep(2000);
	fdObj.findElementByXPath("/html/body/div/nav/div[2]/ul/li[6]/ul/li[2]/a").click(); // click UnIdentified Products
	Thread.sleep(2000);
	fdObj.findElementByXPath("/html/body/div/div/div/div/div/div[3]/div/div/div/table/tbody/tr[1]/td[1]/a").click(); // click Product Name 'Mobile'
	Thread.sleep(2000);		
	fdObj.findElementByXPath("//*[@class='btn btn-primary imageupload']").click(); // clicking Upload Product Image
	Thread.sleep(5000);
 	fdObj.findElementById("custom-fileupload").sendKeys("/home/delhivery/Pictures/AttributeIssue1.png"); // uploading the image path
 	fdObj.findElementByXPath("//*[@id='id_caption']").sendKeys("Attributes"); // entering image caption
 	
 	WebElement Is_Primary_Image=fdObj.findElementByName("isprimary"); // selecting IsPrimaryImage as No from drop-down
    Select IsPrimaryImage=new Select(Is_Primary_Image);
    IsPrimaryImage.selectByValue("True");
    Thread.sleep(2000);
    
    fdObj.findElementByXPath("//*[@class='btn btn-primary fileupload']").click();
    Thread.sleep(2000);
    fdObj.findElementByXPath("//*[@class='btn btn-default cancel-modal']").click();
    Thread.sleep(6000);
 		
    fdObj.quit();
		}

}
