package webdriverbasics.aug12_08_2015;

import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;

public class Dashboard {
	
	public Dashboard(String Company, int a){
		System.out.println("Selenium");
		System.out.println(Company);
	}

	public static void main(String[] args) throws InterruptedException, Exception {
		// TODO Auto-generated method stub

		// WebDriverBasics a=new WebDriverBasics("abcd", 34);
		
		FirefoxDriver fdObj = new FirefoxDriver ();  ///launching browser
		fdObj.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		fdObj.manage().window().maximize();

		fdObj.get("http://qa-pcms.delhivery.com");  ///Opening URL
		
		WebElement uname_we=fdObj.findElementByName("email");
		uname_we.clear();    //// clearing username field
		uname_we.sendKeys("charles.wilson");   /// Enter Username
		Thread.sleep(1000);
		
		fdObj.findElementByXPath("//input[@name='password']").clear();  // clearing password field
		fdObj.findElementByXPath("//input[@name='password']").sendKeys("pcms123");   /// Enter password
		Thread.sleep(1000);
		
		fdObj.findElementByXPath("//input[@value='login']").click();  // Click Login

		fdObj.findElementByXPath("//*[@value='1']").click();  // selecting Godam Application Account from drop down
		Thread.sleep(1000);
		fdObj.findElementByXPath("//*[@class='btn btn-lg btn-default btn-block']").click(); // click Submit
		Thread.sleep(2000);

		String pageurl=fdObj.getCurrentUrl();
		System.out.println(pageurl);
		boolean st_str= pageurl.equalsIgnoreCase("https://qa-pcms.delhivery.com/");
		if(st_str==true) {
			 System.out.println("Passed");
		}else{
			System.out.println("Failed");
		}
		
		Thread.sleep(5000);
		fdObj.close();
	}

}


