package webdriverbasics.aug12_08_2015;

import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class Categories {
	
	public Categories(String Company, int a){
		System.out.println("Selenium");
		System.out.println(Company);
	}

	public static void main(String[] args) throws InterruptedException, Exception {
		// TODO Auto-generated method stub

		// WebDriverBasics a=new WebDriverBasics("abcd", 34);
		
		FirefoxDriver fdObj = new FirefoxDriver ();  ///launching browser
		fdObj.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		fdObj.manage().window().maximize();

		fdObj.get("http://qa-pcms.delhivery.com");  ///Opening URL
		
		WebElement uname_we=fdObj.findElementByName("email");
		uname_we.clear();    //// clearing username field
		uname_we.sendKeys("charles.wilson");   /// Enter Username
		
		fdObj.findElementByXPath("//input[@name='password']").clear();  // clearing password field
		fdObj.findElementByXPath("//input[@name='password']").sendKeys("pcms123");   /// Enter password
		
		fdObj.findElementByXPath("//input[@value='login']").click();  // Click Login

		fdObj.findElementByXPath("//*[@value='1']").click();  // selecting Godam Application Account from drop down
		fdObj.findElementByXPath("//*[@class='btn btn-lg btn-default btn-block']").click(); // click Submit
		
        fdObj.findElementByXPath("/html/body/div/nav/div[2]/ul/li[4]/a").click(); // click Categories
	 	Thread.sleep(2000);
        fdObj.findElementByXPath("/html/body/div/nav/div[2]/ul/li[4]/ul/li[3]/a").click(); // click 'Map Category with Others'
        Thread.sleep(2000);
        
        WebElement Category_Name=fdObj.findElementByName("pcms_category"); // selecting Electronics in 'Category Name' drop down
              Select categoryObj=new Select(Category_Name);
              categoryObj.selectByValue("65");
              Thread.sleep(2000);

        WebElement ThirdParty_Category_Name=fdObj.findElementByName("third_party_category"); // selecting Uncategorized in 'Third Party Category Name' drop down
              Select ThirdPartyCategoryObj=new Select(ThirdParty_Category_Name);
              ThirdPartyCategoryObj.selectByValue("14, Uncategorized");
              Thread.sleep(2000);
              
        WebElement ThirdParty_Name=fdObj.findElementByName("party"); // selecting D-One in 'Third Party Name' drop down
              Select ThirdPartyName=new Select(ThirdParty_Name);
              ThirdPartyName.selectByValue("1");
              Thread.sleep(2000);
              
        fdObj.findElementByXPath("//*[@value='Map Category']").click();  // click 'Map Category' button
             
        Thread.sleep(5000);
		fdObj.close();
    	
	}

}



